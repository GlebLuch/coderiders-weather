package Parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Extractor {

    Regex regex = new Regex();

    public  String getDateFromString(String stringDate) throws Exception {

        Pattern pattern = Pattern.compile(regex.DATE_REGEX);
        Matcher matcher = pattern.matcher(stringDate);
        if (matcher.find()) {
            return  matcher.group();
        }
        throw new Exception("Cant extract date from string");
    }
}
