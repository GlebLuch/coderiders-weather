package Parser;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Printer {

    public int printValues(Elements values, int index) {
        int iterationCount = 4;
        for (int i = 0; i < 4; i++) {
            Element valueLine = values.get(index + i);
            for (Element td : valueLine.select("td")) {
                System.out.print(td.text() + "  \t");
            }
            System.out.println();
        }
        return iterationCount;
    }
}
